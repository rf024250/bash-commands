$ mkdir -p $HOME/portfolio/week1; cd $HOME/portfolio/week1
The given command creates a directory named "week1" inside $HOME/portfolio/ and changes the current directory to the newly created "week1" directory.
$ cd ~
The command is used to change the current working directory to the home directory of the currently logged-in user 
$ rm -r portfolio
The command rm -r portfolio deletes the directory named "portfolio" along with all its contents. 
$ mkdir -p $HOME/portfolio/week1 & cd $HOME/portfolio/week1
The command mkdir -p $HOME/portfolio/week1 && cd $_ will create a directory named "week1" inside $HOME/portfolio/ and change the current working directory to this newly created "week1" directory.
$ cd ~
The command cd ~ or just cd without any arguments changes the current directory to the user's home directory.
$ rm -r portfolio
The command rm -r portfolio deletes the directory named "portfolio" along with all its contents. 
$ mkdir -p $HOME/portfolio/week1 && cd $HOME/portfolio/week1
The command mkdir -p $HOME/portfolio/week1 && cd $_ creates a directory named "week1" inside $HOME/portfolio/ and changes the current working directory to the newly created "week1" directory.
$ echo "Hello World"
The command prints the text "Hello World"
$ echo Hello, World
The command echo prints the text "Hello, World" to the terminal. 
$ echo Hello, world!
The command "echo Hello, world!" in a terminal prints "Hello, world!" to the console.
$ echo "line one"; echo "Line two"
This command prints "line one" and then immediately prints "line two" in the terminal.
$ echo "Hello, world > readme"
This command attempts to print "Hello, world > readme" to a file named "readme".
$ echo "Hello, world" > readme
This command writes "Hello, world" into a file named "readme".
$ cat readme
This command displays the contents of the "readme" file.
$ example="Hello, World"
This command creates a variable named "example" with the value "Hello, World".
$ echo $example
This command prints the value of the variable "example".
$ echo ’$example’
This command prints the text "$example" because single quotes prevent variable expansion.
$ echo "$example"
This command prints the value of the variable "example" because of the double quotes, allowing variable expansion.
$ echo "Please enter your name."; read example
This command prints "Please enter your name." and then reads the user's input into the variable "example".
$ echo "Hello $example"
This command prints "Hello" followed by the value stored in the variable "example".
$ three=1+1+1;echo $three
This command calculates (1+1+1) and assigns the result to the variable "three", then prints the value of "three".
$ bc
The "bc" command is a terminal-based calculator for performing precise arithmetic calculations.To quit the "bc" calculator, you can use the "quit" command.
$ echo 1+1+1 | bc
The command "echo 1+1+1 | bc" calculates the sum of 1+1+1 and displays the result in the terminal.
$ let three=1+1+1;echo $three
This command sets a variable "three" to the sum of 1+1+1 and then prints its value using "echo".
$ echo date
This command prints the text "date" to the terminal.
$ cal
The "cal" command displays the calendar for the current month in the terminal.
$ which cal
"which cal" finds the path of the "cal" command in your system.
$ /bin/cal
"/bin/cal" executes the "cal" command from the "/bin" directory in the terminal.
$ $(which cal)
This command executes the "cal" command, finding its path dynamically using "which".
$ ‘which cal‘
"`which cal`" finds and displays the path of the "cal" command in the terminal.
$ echo "The date is $(date)"
This command prints the current date and time using the "date" command within the text, surrounded by double quotes.
$ seq 0 9
This command generates and prints numbers from 0 to 9 in the terminal.
$ seq 0 9 | wc -l
This command generates numbers from 0 to 9 using "seq" and then counts the lines using "wc -l".
$ seq 0 9 > sequence
This command generates numbers from 0 to 9 and saves them in a file named "sequence".
$ wc -l < sequence
This command counts the number of lines in the "sequence" file and displays the count in the terminal.
$ for I in $(seq 1 9) ; do echo $I ; done
This command prints numbers from 1 to 9 using a "for" loop in the terminal.
$ (echo -n 0 ; for I in $(seq 1 9) ; do echo -n +$I ; done ; echo) | bc
This command calculates the sum of numbers from 0 to 9 using the "bc" calculator in the terminal.
$ echo -e ‘#include <stdio.h>\nint main(void) \n{\n printf(“Hello World\\n”);\n return 0;\n}’ > hello.c
This command creates a C source code file named "hello.c" with a basic "Hello World" program inside.
$ cat hello.c
This command displays the content of the "hello.c" file in the terminal.
$ gcc hello.c -o hello
This command compiles the "hello.c" file into an executable named "hello" using the GCC compiler.
$ ./hello
This command runs the compiled "hello" executable in the terminal.
